let boo = {


    mostrarAlerta: function (message, type) {


        // Crea un elemento de notificación de Bootstrap dinámicamente        
        var alertDiv = document.createElement('div');
        alertDiv.id = 'idalerta';  
        alertDiv.className = 'alert fade show ' + type + ' alert-dismissible'; // Agrega las clases necesarias
        alertDiv.setAttribute('role', 'alert');
    
        // Div para el mensaje
        var messageDiv = document.createElement('div');
        messageDiv.className = 'pe-5'
        messageDiv.innerHTML = message;
        alertDiv.appendChild(messageDiv);
    
        // Div para el botón de cerrar
        var closeButton = document.createElement('button');
        closeButton.type = 'button';
        closeButton.className = 'btn-close';
        closeButton.setAttribute('data-bs-dismiss', 'alert');
        closeButton.setAttribute('aria-label', 'Close');
        alertDiv.appendChild(closeButton);
            

        // Busca el elemento por su id
        var alertaElemento = document.getElementById(alertDiv.id);
        // Verifica si el elemento existe antes de intentar eliminarlo
        if (alertaElemento) {
            alertaElemento.remove();
        }

        // Se agrega clases de posicionamiento
        alertDiv.classList.add('position-absolute');             
        alertDiv.classList.add('bottom-0', 'start-0');
        alertDiv.classList.add('m-3');


        document.body.appendChild(alertDiv);

        // Agrega estilos para posicionar la alerta en la parte inferior izquierda
        /*
        alertDiv.style.position = 'fixed';
        alertDiv.style.bottom = '10px';
        alertDiv.style.left = '10px';
        */

        // Muestra la notificación
        var bootstrapAlert = new bootstrap.Alert(alertDiv);
    

    },
    




    mostrarToast : function (message, type) {

        // Crea un elemento de toast de Bootstrap dinámicamente
        let toastDiv = document.createElement('div');
        toastDiv.className = 'toast align-items-center text-white border-0 bg-light ' + type;

        // Aplica el color de fondo directamente con JavaScript
        

    
        let toastInnerDiv = document.createElement('div');
        toastInnerDiv.className = 'd-flex  bg-danger';
        /*toastInnerDiv.style.backgroundColor = 'rgb(220, 53, 69)'; */

        
    
        // Div para el cuerpo del toast
        let toastBodyDiv = document.createElement('div');
        toastBodyDiv.className = 'toast-body bg-transparent';
        toastBodyDiv.innerHTML = message;
        toastInnerDiv.appendChild(toastBodyDiv);
    
        // Botón de cerrar
        let closeButton = document.createElement('button');
        closeButton.type = 'button';
        closeButton.className = 'btn-close btn-close-white me-2 m-auto';
        closeButton.setAttribute('data-bs-dismiss', 'toast');
        closeButton.setAttribute('aria-label', 'Close');
        toastInnerDiv.appendChild(closeButton);
    
        // Agrega el div interior al div de toast
        toastDiv.appendChild(toastInnerDiv);
    
        // Agrega estilos para posicionar el toast en la parte inferior derecha
        toastDiv.style.position = 'fixed';
        toastDiv.style.bottom = '10px';
        toastDiv.style.right = '10px';
    
        // Agrega el elemento de toast al cuerpo del documento
        document.body.appendChild(toastDiv);
    
        // Crea y muestra el toast
        var toast = new bootstrap.Toast(toastDiv);
        toast.show();
    },

};

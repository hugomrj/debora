

function mostrar_menu_lateral(){    
    

    var mainmenu = document.getElementById('mainmenu');              
    mainmenu.onclick = function(  )
    {          

        var anchoPantalla = window.innerWidth;
        var idsideleft = document.getElementById('idsideleft');
        var idmain = document.getElementById('idmain');


        var menu_oculto = "no"
        // Obtener el estilo computed del elemento
        var estilo = window.getComputedStyle(idsideleft);

        // Verificar si el elemento está oculto o tiene un ancho cero
        if (estilo.display === 'none' || idsideleft.offsetWidth === 0) {
            menu_oculto = "si";
        } else {
            menu_oculto = "no";
        }


        if (anchoPantalla < 768) 
        {            

            if (menu_oculto == "no")
            {
                idsideleft.style.flexGrow = "0";     
                idsideleft.style.display = "none";

                idmain.style.display = 'initial';
                idmain.style.flexGrow = "10";     



                menu_oculto = "si"
            }
            else
            {
                idsideleft.style.display = 'initial';
                idsideleft.style.flexGrow = "10";     

                idmain.style.display = 'none';
                idmain.style.flexGrow = "0";     

                menu_oculto = "no"
            }

        } 
        else 
        {        
            
            if (menu_oculto == "no")
            {
                idsideleft.style.flexGrow = "0";     
                idsideleft.style.display = "none";

                idmain.style.display = 'initial';
                idmain.style.flexGrow = "10";    

                menu_oculto = "si"
            }
            else
            {
                idsideleft.style.display = 'initial';
                idsideleft.style.flexGrow = "1"; 
                
                idmain.style.display = 'initial';
                idmain.style.flexGrow = "9";                    
                
                menu_oculto = "no"
            }
        }


    };       
    

}





function cargar_menu() {
    
    var xhr = new XMLHttpRequest();
    var url = './menu/menu1.html';

    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4 && xhr.status === 200) {
            document.getElementById('menu_acordeon').innerHTML = xhr.responseText;
        }
    };

    xhr.open('GET', url, true);
    xhr.send();
}





// Definición de la función principal
function handleRoute() {
    
    var menuContainer = document.getElementById('menu_acordeon');    
    
    menuContainer.addEventListener('click', function (event) {
        // Verificar si el clic fue en un elemento <li> con el atributo data-section
        var listItem = event.target.closest('a[data-section]');
        if (listItem) {            
            var sectionName = listItem.getAttribute('data-section');                        
            loadSection(sectionName);
        }
    });
}
function loadSection(sectionName) {
    var contentContainer = document.getElementById('idmain');
    contentContainer.innerHTML = '';

    switch (sectionName) {
        case 'mn00':
            contentContainer.innerHTML = 'pagina de inicio';
            break;
        case 'mn14': 
            fetch('componentes/factura/condicionventa/lista.html')
            .then(response => response.text())
            .then(html => {
                contentContainer.innerHTML = html;
            })
            .catch(error => {
                console.error('Error al cargar la página:', error);
            });    
            break;
        default:
            break;
    }
}